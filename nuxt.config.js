const axios = require('axios')

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  globalName: 'originbags',
  ssr: true,
  server: {
  port: 3021 // default: 3000
},
  router: {

    routeNameSplitter: '/',
    //prefetchPayloads: false
  //  mode: 'hash'
  //  middleware: 'routeDelHash'
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'ORIGINBAGS',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        name: 'yandex-verification',
        content: ''
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Интернет-магазин сумок'
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: 'сумки, из кожи, женские сумки, мужские сумки'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }],
    script: [
      {
       src: "//code-ya.jivosite.com/widget/oILccDRubV",
       async: true
     },
//code-ya.jivosite.com/widget/oILccDRubV
     ],
  },
  /*publicRuntimeConfig: {

    axios: {
//baseURL: 'http://api.elborso/'
    baseURL: 'https://api.originbags.ru'
    }
  },*/
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/app.scss'
  //  '@node_modules/vue-slick-carousel/dist/vue-slick-carousel'
  ],

  loadingIndicator: {
    name: 'circle',
    color: 'black',
    background: 'white'
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  //  { src: '~/plugins/b24.js'},
    {
      src: '~/plugins/persistedState.client.js'
    },
    { src: './plugins/vue-slick.js' },
    {
      src: '~/plugins/switch.js'
    },
    {
      src: '~/plugins/ranger.js',
      ssr: false
    },
    {  src: '~/plugins/mask.js'},
    {  src: '~/plugins/scrollvue.js'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  //   '@nuxtjs/google-analytics'
   ],
  /* googleAnalytics: {
    id: 'UA-196558151-1'
  },*/
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  //  '@nuxtjs/google-gtag',

    ['@nuxtjs/recaptcha', {
      hideBadge: true,
      siteKey: ' ',

      version: 2,
    }],
    ['vue-scrollto/nuxt', { duration: 300 }],

  /*   ['nuxt-facebook-pixel-module', {
      track: 'PageView',
      pixelId: '860807558002697',
      autoPageView: true,
      disabled: false,
      debug:true
    }],
    [
     '@nuxtjs/yandex-metrika',
     {
       id: '69095230',
       webvisor: true,
       clickmap:true,
        trackHash:true,
        trackLinks:true,
       accurateTrackBounce:true,
     }
   ],*/
    // https://go.nuxtjs.dev/bootstrap
    ['bootstrap-vue/nuxt', {
      componentPlugins: [
      'LayoutPlugin',
      'FormRating',
      'FormCheckboxPlugin',
      'FormInputPlugin',
      'FormRadioPlugin',
      'ModalPlugin',
      'CarouselPlugin',
      'DropdownPlugin',
      'BreadcrumbPlugin',
      'NavPlugin',
      'NavbarPlugin',
      'CollapsePlugin',
      'InputGroupPlugin',
      'ButtonPlugin',
      'TooltipPlugin',
      'CardPlugin',
      'FormDatepickerPlugin'

    ],
      css: false,
      icons: false
    }],
    ['@nuxtjs/axios'],
    ['nuxt-i18n'],
     '@nuxtjs/sitemap',
     '@nuxtjs/proxy',
      '@nuxtjs/axios'
  ],
  proxy: {
'/api/': { target:  'http://micro/'}
},
axios: {
// baseURL: '/api/v1',
proxy: true,
prefix: '/api/market.v1'
},
/*  'google-gtag': {
      id: 'AW-379798474',
      debug: true,
    },*/
  sitemap: {
    path: '/sitemap.xml',
     hostname: 'https://originbags.ru',

     i18n: true,
     i18n: {
       locales: ['ru'],
       routesNameSeparator: '___'
     },

    sitemaps: [
      {
        exclude: [
        '/analytics'
      ],
        path: '/sitemapcatalog.xml',
        routes: async () => {
           const { data } = await axios.get('https://api.originbags.ru')
           return data.map((product) => `/catalog/${product.category}/${product.title}/${product.id}`)
         }
      },
      {
        exclude: [
        '/analytics'
      ],
        path: '/sitemapblog.xml',
        routes: async () => {
           const { data } = await axios.get('https://api.originbags.ru/blog')
           return data.map((product) => `/blog/${product.id}`)
         }
      }]
     // options
   },/* */

  i18n: {
    locales: [
      {
        code: 'ru',
        file: 'ru-RU.js'
      }

    ],
    lazy: true,
    useCookie: true,
    langDir: 'locality/',
    defaultLocale: process.env.LANG_SERV,
  },
  dev: {
   useEslint: false,

 },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    //analyze: true,
  //  extractCSS: true,
    splitChunks: {
     layouts: false,
     pages: true,
     commons: true
   },
    publicPath: '/nuxt/',
    transpile: ['gsap'],

    babel: {
      compact: true
    }
  }
}
