#!/bin/sh
cd /home/sasha/sites/elborso.ru
if [[ `git pull` != "Already up to date." ]]; then
 npm i >/dev/null
 npm run build >/dev/null
 /usr/local/lib/node_modules/pm2/bin/pm2 restart elborso
fi
