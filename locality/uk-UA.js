export default {
  home: 'Головна',
  catalog: 'Каталог',
  zhenskie_sumki: 'Жіноча сумка | Жіночі сумки',
    muzhskie_sumki: 'Чоловіча сумка | Чоловічі сумки',
  accessories: 'Аксесуари',
  sale: 'Акції',
  callback: 'Замовити дзвінок',
  daily: 'Щодня',
  inCart: 'В кошик',
  buynow: 'Придбати зараз',
  freeshipping: 'Безкоштовна доставка',
  payaftinspection: 'Оплата після огляду',
  shippingterms: 'Умови безкоштовної доставки',
  bestseller : 'Хiт продажу',
  best: 'Хіт',
  about: 'Про нас',
  deliveryPay: 'Доставка і оплата',
  delivery: 'Доставка',
  contacts: 'Контакти',
  review: 'Відгуки',
    allmodels: 'Всі моделі',
  right: 'Всі права захищені',
  filter: 'Фільтр',
  big: 'Большие',
  middle: 'Повсед..',
  hobo: 'Хобо',
  small: 'Маленькие',
  port: 'Портфель',
    rukzak: 'Рюкзак',
    shoper:' Шоперы',
  crosmen: 'На плече',
  barset: 'Барсетка',
  clear: 'Очистити',
  search: 'Я шукаю...',
  colorTitle: 'Колір',
  color:{
    red: 'Червоний| Червона',
    black: 'Чорний|Чорна',
    green: 'Зелений|Зелена',
    blue: 'Синій|Синя',
    yellow: 'Жовтий|Жовта',
    gray: 'Сірий|Сіра',
    taup: 'Таупо',
    bordo: 'Бордовий|Бордова',
    beige: 'Бежевий|Бежева',
    snake: 'Змія',
    pistachio: 'Фiсташка',
    grey: 'Сіра',
    cappuccino: 'Капучино',
    brown: 'Коричнева',
    crocodile:'Крокодил',
      white: 'Білий',
    rose: 'Рожевий'
  },
  month:{
    April: 'Квітень|Квітня',
    May: 'Травень|Травня',
    Jun: 'Червень|Червень',
    July: 'Липень|Липень',
    August: 'Серпень|Серпня',
    September: 'Вересень|Вересня',
    October: 'Жовтень|Жовтня',
    November: 'Листопад|Листопада',
    December: 'Грудень|Грудня',
    January: 'Січень|Січня',
    February: 'Лютий|Люті',
    March: 'Березень|Березня'
  },
  article:'Арт.',
    sortby:'Сортувати за',
  popularity: 'Популярності',
  byprice: 'Ціні',
  price: 'Ціна',
  result: 'Нічого не знайдено',
  aboutProduct: 'Інформація',
  characteristic: 'Характеристики',
  accaunt: 'Профіль',
  favorites: 'Вибране',
  cart: 'Кошик',
  customerdata: 'Дані клієнта',
  name: 'Iм\'я',
  phonenumber: 'Номер телефону',
  adress: 'Адреса',
  errorPhone: 'Номер телефону некоректно!',
  pay: 'Купити',
  total: 'Итого',
  delete: 'вилучити',
  send: 'Надіслати',
  privacy: 'Політика конфіденційності',
    showmore: 'показати ще',
    loginorregistration: 'Увійти або зареєструватися',
      getcod: 'Отримати код',
    sendcod: 'Надіслати код',
    cod: 'Код',
      again: 'знову',
    shopofbags: 'Магазин сумок і аксесуарів зі шкіри',
    new: 'Новинка',
    Text: 'Текст',
    crossbody: 'Кросс-бодi',
    messenger:'Мессенджер',
      baguette: 'Багет',
      instock:'В наявності',
      outofstock: 'Немає в наявності',
      typeBag: 'Тип сумки',
      bonuse: 'При реєстрації на сайті Вам будуть доступні привітальні 100 бонусів, які Ви можете використовувати при покупці',
      blog: 'Блог'
}
