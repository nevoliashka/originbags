export default {
  home: 'Главная',
  catalog: 'Каталог',
  zhenskie_sumki: 'Женская сумка | Женщинам',
  muzhskie_sumki: 'Мужская сумка | Мужчинам',
  accessories: 'Аксессуары',
  sale: 'SALE',
  callback: 'Заказать звонок',
  daily: 'Ежедневно',
  inCart: 'В корзину',
  buynow: 'Купить сейчас',
  freeshipping: 'Бесплатная доставка',
  payaftinspection: 'Оплата после осмотра',
  shippingterms: 'Условия бесплатной доставки',
  bestseller : 'Популярные',
  best: 'Хит',
  about: 'О нас',
  deliveryPay: 'Доставка и Оплата',
  delivery: 'Доставка',
  contacts: 'Контакты',
  review: 'Отзывы',
    allmodels: 'Все модели',
  right: 'Все права защищены',
  filter: 'Фильтр',
  big: 'Большие',
  middle: 'Повсед..',
  hobo: 'Хобо',
  small: 'Маленькие',
  port: 'Портфель',
  rukzak: 'Рюкзак',
  shoper:' Шоперы',
  crosmen: 'На плечо',
  barset: 'Барсетка',
  clear: 'Очистить',
  search: 'Поиск...',
  colorTitle: 'Цвет',
  color:{
    red: 'Красный| Красная',
    black: 'Черный|Черная',
    green: 'Зеленый|Зеленая',
    blue: 'Синий|Синяя',
    yellow: 'Желтый|Желтая',
    gray: 'Серый|Серая',
    taup: 'Тауп',
    bordo: 'Бордовый|Бордовая',
    beige: 'Бежевый|Бежевая',
    snake: 'Змея',
    pistachio: 'Фисташка',
    grey: 'Серая',
    cappuccino: 'Капучино',
    brown: 'Коричневая',
    cappuccino: 'Капучино',
    crocodile:'Крокодил',
    white: 'Белый',
    rose: 'Розовый'
  },
  month:{
    April: 'Апрель|Апреля',
    May: 'Май|Мая',
    Jun: 'Июнь|Июня',
    July: 'Июль|Июля',
    August: 'Август|Августа',
    September: 'Сентябрь|Сентября',
    October: 'Октябрь|Октября',
    November: 'Ноябрь|Ноября',
    December: 'Декабрь|Декабря',
    January: 'Январь|Января',
    February: 'Февраль|Феврал',
    March: 'Март|Марта'
  },
  article:'Арт.',
  sortby:'Сортировать по',
  popularity: 'Популярности',
  byprice: 'Цене',
  price: 'Цена',
  result: 'Ничего не найдено',
  aboutProduct: 'О товаре',
  characteristic: 'Характеристики',
  accaunt: 'Аккаунт',
  favorites: 'Избранное',
  cart: 'Корзина',
  customerdata: 'Данные клиента',
  name: 'Имя',
  phonenumber: 'Номер телефона',
  adress: 'Адрес',
  errorPhone: 'Номер телефона не корректный!',
  pay: 'Купить',
  total: 'Итого',
  delete: 'удалить',
  send: 'Отправить',
  privacy: 'Политика конфиденциальности',
    showmore: 'Показать еще',
    loginorregistration: 'Войти или зарегистрироваться',
    getcod: 'Получить код',
    sendcod: 'Отправить код',
    cod: 'Код',
    again: 'снова',
    shopofbags: 'Интернет-магазин сумок и аксессуаров ручной работы',
    new: 'Новинка',
    Text: 'Текст',
    crossbody: 'Кросс-боди',
    messenger:'Мессенджер',
      baguette: 'Багет',
      instock:'В наличии',
      outofstock: 'Нет в наличии',
      typeBag: 'Тип сумки',
      bonuse: 'При регистрации на сайте Вам будут доступны приветственные 100 бонусов, которые Вы можете использовать при покупке',
      Refundorexchange: 'Возврат или обмен',
      blog: 'Блог',
      view: 'Смотреть',
      description:'Интернет-магазин сумок из кожи',
      Naturalleather:'Natural </br> leather',
      ManualWork:'Manual </br> Work',
      Guaranteequality:'Guarantee </br> quality',
      Paymentuponreceipt:'Payment upon receipt',
      Freechargedelivery:'Free of charge delivery',
      Quickview:'Быстрый просмотр',
      genuineleather:'genuine leather',
      only:'only',
      byElborso:'by OriginBags',
      valuta:'руб.',
      curs: '1.13',
      Boughtover:'bought over',
      times:'times',
      Peculiarities:'Peculiarities',
      Tags:'Tags',
      Package:'Package',
      Spunbondnonwovenfabric:'Spunbond nonwoven fabric',
      Derby:'Derby',
      Moreinformproduct:'Еще больше информации о продукте',
      writefeedback:'Оставить отзыв',
      notnecessary: 'Не обязательно',
      Paymentbycard:'Payment by card',
      Promocode:'Промокод',
      Apply:'Применить',
      bonuses:'бонусы',
      use:'использовать'
}
