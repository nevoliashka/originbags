export const state = () => ({
  contacts:[],
  list: [],
  wishlist: [],
  sum: 0,
  token:'',
  allviewcheck:true
})

export const mutations = {
  add(state, ord) {

    let search = ord.id;
    let cityId = state.list.findIndex(city => city.id === search)

    if(cityId!=-1){
  //    console.log(cityId)
      state.list[cityId]['count']+=1
      state.sum+=ord.price
    }else{
      state.list.push(ord)
      state.sum+=ord.price
    }

  },
///Избранное
  addWishlist(state, ord) {
    let search = ord.id;
    let cityId = state.wishlist.findIndex(city => city.id === search)
    if(cityId==-1){
      state.wishlist.push(ord)
    }
  },
  addAllviewchecked(state, ord) {
    state.allviewcheck=ord
  },
  removeWishlist(state,  { todo } ) {
    let arr=state.wishlist

    for (let index = 0; index < arr.length; index++) {
       if (arr[index].id == todo){arr.splice(index, 1)}
        }


  },

  addToken(state, ord) {
   state.token=ord
  },
  removeToken(state) {
   state.token=''
  },

  ///Избранное
  addCount(state, ord){
    state.list[ord.index]['count']=ord.count

let arr=state.list
  state.sum=0
for (let index = 0; index < arr.length; index++) {
      state.sum+=(arr[index].price*arr[index].count);

    }

    //  state.list[ord.index]["count"]=ord.count
  },
  addContact(state, ord) {
    state.contacts=ord
  },
  remove(state,  { todo } ) {

    let arr=state.list
      state.sum=0

    for (let index = 0; index < arr.length; index++) {
       if (arr[index].id == todo){arr.splice(index, 1)}
        }

    for (let index = 0; index < arr.length; index++) {
       if (arr[index].id == todo){arr.splice(index, 1)}
          state.sum+=(arr[index].price*arr[index].count);
        }

  },
  toggle(state, todo) {
    todo.done = !todo.done
  }
}
